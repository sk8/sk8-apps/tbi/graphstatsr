# graphstats v2.2.0

ARG R_IMAGE=rocker/r-ver:latest
FROM rocker/r-ver:4.4.2
RUN echo "rocker/r-ver:4.4.2" > docker_image_version
ARG AUTHOR="SK8 team"

LABEL 	org.opencontainers.image.authors="SK8 Team" \
        org.opencontainers.image.licenses="GPL-2.0-or-later" \
      	org.opencontainers.image.source="https://forgemia.inra.fr/sk8/team/ressources/docker/dockerfile" \
      	org.opencontainers.image.vendor="SK8 Project" \
        org.opencontainers.image.description="A simple configuration to run a R-Shiny App with renv" 


## Add here libraries dependencies
## there are more libraries that are needed here
## Valeur par defaut de RENV
ARG SYS_LIB=""
## Recuperation de la version de renv en parametre
RUN apt-get update && apt-get install -y --no-install-recommends \
    sudo \
    gdebi-core \
    locales \
    git \
    openssh-client \
    libssh2-1-dev \
    libgit2-dev \
    zlib1g-dev \
    make \
    pandoc \
    libssl-dev \
    libcurl4-openssl-dev \
    libnode-dev \
    libgdal-dev \
    gdal-bin \
    libgeos-dev \
    libproj-dev \
    libsqlite3-dev \
    libicu-dev \
    cmake \
    git-core \
    libgit2-dev \
    libxml2-dev \
    zlib1g-dev \
    libgmp3-dev \
    libmpfr-dev \
    libfontconfig1-dev \
    libharfbuzz-dev \
    libfribidi-dev \
    libfreetype6-dev \
    libpng-dev \
    libtiff5-dev \
    libjpeg-dev

## Change timezone
ENV CONTAINER_TIMEZONE Europe/Paris
ENV TZ Europe/Paris

RUN sudo echo "Europe/Paris" > /etc/timezone
RUN echo "fr_FR.UTF-8 UTF-8" >> /etc/locale.gen \
  && locale-gen fr_FR.UTF-8 \
  && /usr/sbin/update-locale LANG=fr_FR.UTF-8

ENV LC_ALL fr_FR.UTF-8
ENV LANG fr_FR.UTF-8

## Change shiny user rights
## USER shiny
RUN useradd -ms /bin/bash shiny
RUN passwd shiny -d


## App name by default, the name is set at docker build dynamically using project name
ARG APP_NAME="superApp"
ENV APP_NAME graphstatsr

# copy project into shiny home
RUN mkdir -p /home/shiny/graphstatsr
WORKDIR /home/shiny/graphstatsr
ADD . /home/shiny/graphstatsr

# Valeur par defaut de RENV
ARG RENV_VERSION=0.15.3
# Recuperation de la version de renv en parametre
ENV RENV_VERSION 1.0.2
# renv cache
ARG RENV_PATHS_CACHE="/home/shiny/graphstatsr/.renv_cache"
ENV RENV_PATHS_CACHE ${RENV_PATHS_CACHE}

## R package dependencies
## Install la derniere version de renv
#RUN Rscript -e "if (!requireNamespace('renv', quietly = TRUE, repos='https//cloud.r-project.org')) install.packages('renv')"
## Install la version de renv utilise
RUN R -e "install.packages('remotes', repos = c(CRAN = 'https://cloud.r-project.org'))"
RUN R -e "remotes::install_version('renv',version='1.0.7', repos=c(CRAN = 'https://cloud.r-project.org'))"
RUN Rscript -e "setwd('/home/shiny/graphstatsr'); options(renv.config.cache.symlinks = FALSE); renv::deactivate(); renv::restore()"

## Install R Packages from another sources
## use : https://github.com/r-lib/remotes
## ex:
## RUN -e "remotes::install_gitlab('sk8/sk8-conf/sk8tools@main', host='forgemia.inra.fr')"


## Clean unnecessary libraries
# remove renv cache if any
RUN rm -rf ${RENV_PATHS_CACHE}
RUN apt-get clean \
  && rm -rf /var/lib/apt/lists/*

RUN chown -R shiny:shiny /home/shiny

USER shiny

EXPOSE 3838

## App name by default, the name is set at docker build dynamically using project name
ARG APP_VERSION="SK8"
ENV APP_VERSION SK8-783863f7

# CMD ["sh", "-c", "Rscript -e \"shiny::runApp('/home/shiny/graphstatsr', port = 3838, host = '0.0.0.0' ) \""]
CMD ["sh", "-c", "Rscript -e \"options('shiny.port'=3838,shiny.host='0.0.0.0');library(graphstatsr);print(tempdir());print(sessionInfo());graphstatsr::run_app() \""]


